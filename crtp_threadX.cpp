/*Copyright (C)
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "FreeRTOS.h"
#include "task.h"

template<typename T>
class ThreadX
{
   public:
      ThreadX( unsigned portSHORT _stackDepth, UBaseType_t _priority, const char* _name = "" )
      {
         xTaskCreate( task, _name, _stackDepth, this, _priority, &this->taskHandle );
      }

      TaskHandle_t GetHandle()
      {
         return this->taskHandle;
      }

      void Main()
      {
         static_cast<T&>( *this ).Main();
      }

   private:
      static void task( void* _params )
      {
         ThreadX* p = static_cast<ThreadX*>( _params );
         p->Main();
      }

      TaskHandle_t taskHandle;
};







class LedX : public ThreadX<LedX>
{
   public:
      LedX( unsigned portSHORT _stackDepth, UBaseType_t _priority, const char* _name,
            uint8_t _port, uint8_t _pin, uint32_t _ticks ) :
         ThreadX{ _stackDepth, _priority, _name },
         port{ _port }, pin{ _pin }, ticks{ _ticks }
      {
         Chip_GPIO_SetPinDIROutput( LPC_GPIO, this->port, this->pin );
      }

      void Main()
      {
         while( 1 )
         {
            Chip_GPIO_SetPinToggle( LPC_GPIO, this->port, this->pin );
            vTaskDelay( this->ticks );
         }
      }

   private:
      uint8_t port;
      uint8_t pin;
      uint32_t ticks;
};


class AnalogX : public ThreadX<AnalogX>
{
   public:
      explicit AnalogX( CHIP_ADC_CHANNEL _channel ) :
         ThreadX{ 128, tskIDLE_PRIORITY, "ANALOG" },
         channel { _channel }
      {
         Chip_ADC_EnableChannel( LPC_ADC, this->channel, ENABLE );
         Chip_GPIO_SetPinDIROutput( LPC_GPIO, 2, 2 );
      }

      void Main()
      {
         // run to completion, then kindly release the CPU ...
         while( 1 )
         {
            Chip_ADC_SetStartMode( LPC_ADC, ADC_START_NOW, ADC_TRIGGERMODE_RISING );

            while( Chip_ADC_ReadStatus(LPC_ADC, this->channel, ADC_DR_DONE_STAT) != SET ) ;

            uint16_t dataADC;
            Chip_ADC_ReadValue(LPC_ADC, ADC_CH0, &dataADC);

            uint32_t dataADC_in_volts = ( 33 * dataADC ) / ( 1024 );

            dataADC_in_volts < 16 ? Chip_GPIO_SetPinOutHigh( LPC_GPIO, 2, 2 ) : Chip_GPIO_SetPinOutLow( LPC_GPIO, 2, 2 );

            vTaskDelay( configTICK_RATE_HZ / 10 );
         }
      }

   private:

      CHIP_ADC_CHANNEL channel;
};

void AnalogInit()
{
   ADC_CLOCK_SETUP_T ADCSetup;
   Chip_IOCON_PinMuxSet(LPC_IOCON, IOCON_PIO0_11, FUNC2);
   Chip_ADC_Init(LPC_ADC, &ADCSetup);
}

//----------------------------------------------------------------------
int main(void)
{

   SystemCoreClockUpdate();
   Board_Init();
   AnalogInit();

   LedX led1X{ 128, tskIDLE_PRIORITY, "ledX", 0, 7, configTICK_RATE_HZ / 4 };

   LedX led2X{ 128, tskIDLE_PRIORITY, "ledX", 3, 3, configTICK_RATE_HZ / 2 };

   AnalogX analog1X{ ADC_CH0 };

   vTaskStartScheduler();

   return 0 ;
}


//-------------------- C plain stuff -------------------------------------------
#ifdef __cplusplus
extern "C"{
#endif

   void vApplicationMallocFailedHook( void )
   {
      taskDISABLE_INTERRUPTS();
      for( ;; );
   }

   void vApplicationIdleHook( void )
   {


   }

   void vApplicationTickHook( void )
   {
   }


   /* configSUPPORT_STATIC_ALLOCATION is set to 1, so the application must provide an
      implementation of vApplicationGetIdleTaskMemory() to provide the memory that is
      used by the Idle task. */
   void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer,
         StackType_t **ppxIdleTaskStackBuffer,
         uint32_t *pulIdleTaskStackSize )

      ivo: /home/fjrg76/freertosprueba11..._exp/prueba11/src/prueba11.cpp Página 4 de 4
      {
         /* If the buffers to be provided to the Idle task are declared inside this
            function then they must be declared static - otherwise they will be allocated on
            the stack and so not exists after this function exits. */
         static StaticTask_t xIdleTaskTCB;
         static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

         /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
            state will be stored. */
         *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

         /* Pass out the array that will be used as the Idle task's stack. */
         *ppxIdleTaskStackBuffer = uxIdleTaskStack;

         /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
            Note that, as the array is necessarily of type StackType_t,
            configMINIMAL_STACK_SIZE is specified in words, not bytes. */
         *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
      }
   /*-----------------------------------------------------------*/

   /* configSUPPORT_STATIC_ALLOCATION and configUSE_TIMERS are both set to 1, so the
      application must provide an implementation of vApplicationGetTimerTaskMemory()
      to provide the memory that is used by the Timer service task. */
   void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer,
         StackType_t **ppxTimerTaskStackBuffer,
         uint32_t *pulTimerTaskStackSize )
   {
      /* If the buffers to be provided to the Timer task are declared inside this
         function then they must be declared static - otherwise they will be allocated on
         the stack and so not exists after this function exits. */
      static StaticTask_t xTimerTaskTCB;
      static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

      /* Pass out a pointer to the StaticTask_t structure in which the Timer
         task's state will be stored. */
      *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

      /* Pass out the array that will be used as the Timer task's stack. */
      *ppxTimerTaskStackBuffer = uxTimerTaskStack;

      /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
         Note that, as the array is necessarily of type StackType_t,
         configTIMER_TASK_STACK_DEPTH is specified in words, not bytes. */
      *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
   }


#ifdef __cplusplus
}
#endif
//------------------------------------------------------------------------------
